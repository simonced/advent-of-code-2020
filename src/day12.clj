(ns day12
  (:require [utils]
            [clojure.string :as str])
  (:import java.lang.Math))

(defn parse-line
  [line]
  (let [[_ letter number] (re-find #"(\w)(\d+)" line)]
    ;; {:letter letter :number number}
    [letter (read-string number)]
    )
  )

(defn parse-input
  [input]
  (->> input
       (str/split-lines)
       (map parse-line)))

(def demo-input
  (parse-input "F10
N3
F7
R90
F11"))

(def real-input (utils/load-input 12 parse-input))

;;; simply checking the problem input to see if all L and R are in 90 deg increments...
(comment
  "Checking the rotation inputs"
  (->> real-input
      (filter (fn [[letter _]] (or (= "R" letter) (= "L" letter))))
      (map second)
      frequencies
      keys
      sort
      ;; (map #(mod % 90))
      ;; (remove #(= 0 %))
      )
  )


(defn angle->direction
  [angle]
  (case (mod angle 360)
    0 "N"
    90 "E"
    180 "S"
    270 "W"))


(defn direction->angle
  [direction]
  (case direction
    "N" 0
    "E" 90
    "S" 180
    "W" 270))


(defn ship-rotate
  [ship [side deg]]
  (let [angle-mul (case side "R" 1 "L" -1)
        angle (* angle-mul deg)
        current-facing (:facing ship)
        new-facing (->> current-facing direction->angle (+ angle) angle->direction)]
    (assoc ship :facing new-facing)
    )
  )


(defn ship-forward
  "Moves a ship (not only forward"
  [ship [direction distance]]
  (case direction
    "F" (recur ship [(:facing ship) distance])
    "N" (update-in ship [:travel :y] + distance)
    "E" (update-in ship [:travel :x] + distance)
    "S" (update-in ship [:travel :y] - distance)
    "W" (update-in ship [:travel :x] - distance)
    )
  )


(defn ship-gps
  "Moves the ship regarding the instructions"
  [ship [instruction _ :as gps]]
  (case instruction
    "L" (ship-rotate ship gps)
    "R" (ship-rotate ship gps)
    (ship-forward ship gps))
  )

;; ==================================================


(def ship {:facing "E"
           :travel {:x 0 :y 0}
           :waypoint {:x 10 :y 1}})


(comment 
  (-> ship
      (ship-forward ,,, ["F" 20])
      (ship-rotate  ,,, ["L" 180])
      (ship-forward ,,, ["N" 20])
      )

  (-> ship
      (ship-gps ["F" 20])
      (ship-gps ["R" 90])
      )
  )


(defn solve1
  [input]
  (let [ship-final (reduce ship-gps ship input)
        {dist-x :x dist-y :y} (:travel ship-final)]
    (+ (Math/abs dist-x) (Math/abs dist-y))
    ))


(comment 
  (solve1 demo-input) ; 25
  )

;; ==================================================


(defn ship-to-waypoint
  "fwd-amt is the number of times to 'jump' toward the waypoint"
  [ship fwd-amt]
  (let [wx (-> ship :waypoint :x)
        wy (-> ship :waypoint :y)]
    (-> ship 
        (update-in ,,, [:travel :x] + (* fwd-amt wx))
        (update-in ,,, [:travel :y] + (* fwd-amt wy))
        )))

(comment
  (ship-to-waypoint ship 3)
  )


(defn rotate-coords
  "Rotate clock-wise"
  [[x y] angle]
  (case angle
    90 [y (* -1 x)]
    180 [(* -1 x) (* -1 y)]
    270 [(* -1 y) x]))


(defn rotate-coords-ccw
  "Rotate counter clock-wise"
  [[x y] angle]
  (case angle
    270 [y (* -1 x)]
    180 [(* -1 x) (* -1 y)]
    90 [(* -1 y) x]))


(defn waypoint-rotate
  [ship [direction angle]]
  (let [{wx :x wy :y} (:waypoint ship)
        [new-x new-y] (case direction
                        "R" (rotate-coords [wx wy] angle)
                        "L" (rotate-coords-ccw [wx wy] angle))]
    (-> ship
        (assoc-in ,,, [:waypoint :x] new-x)
        (assoc-in ,,, [:waypoint :y] new-y))
    ))


(comment
  (waypoint-rotate ship ["R" 90])
  )


(defn waypoint-move
  [ship [direction distance]]
  (case direction
    "N" (update-in ship [:waypoint :y] + distance)
    "E" (update-in ship [:waypoint :x] + distance)
    "S" (update-in ship [:waypoint :y] - distance)
    "W" (update-in ship [:waypoint :x] - distance)
    )
  )

(comment
  (waypoint-move ship ["N" 10])
  (waypoint-move ship ["E" 10])
  (waypoint-move ship ["S" 10])
  (waypoint-move ship ["W" 10])
  )

;; needed:
;; - [X] ship forward (F)
;; - [X] rotate waypoint (R or L)
;; - [X] move waypoint (N, E, S or W)


(defn ship-gps2
  [ship [instruction distance :as gps]]
  (let [new-ship (cond
          (= "F" instruction) (ship-to-waypoint ship distance)
          (#{"N" "E" "S" "W"} instruction) (waypoint-move ship gps)
          (#{"R" "L"} instruction) (waypoint-rotate ship gps))]
    ;; (println gps new-ship)
    new-ship
    ))


(defn solve2
  [input]
  (->> input
       (reduce ship-gps2 ship ,,,)
       :travel
       ((fn [{x :x y :y}] (+ (Math/abs x) (Math/abs y))))
       ))


(defn run [& _]
  (println "Day 12 solution 1:" (solve1 real-input)) ; 1687
  (println "Day 12 solution 2:" (solve2 real-input)) ; 20873
  )
