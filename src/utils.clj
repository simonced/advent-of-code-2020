(ns utils
  (:require [clojure.string :as str]))

(defn load-input
  "Simply loads a text file and apply f to its content."
  [day-num f]
  (->> (str "input/day" day-num ".txt")
       slurp
       f))

(defn input->list
  "Makes an input into a list of lines."
  [input_]
  (str/split-lines input_))
