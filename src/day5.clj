(ns day5
  (:require utils))

(def demo-input [
                 "FBFBBFFRLR" ; row 44, column 5, seat ID 357
                 "BFFFBBFRRR" ; row 14, column 7, seat ID 119
                 "FFFBBBFRRR" ; row 14, column 7, seat ID 119
                 "BBFFBBFRLL" ; row 102, column 4, seat ID 820
                 ])

;;; memo: F and L mean lower half,
;;;       B and R mean upper half.

(def real-input
  (utils/load-input 5 utils/input->list))


(defn- keep-low?
  [letter]
  (or (= letter \F) (= letter \L)))


(defn- get-interval
  [[mini maxi] letter]
  (let [middle (-> (- maxi mini)
                   inc
                   (/ ,,, 2)
                   dec)]
    (if (keep-low? letter)
      [mini (+ mini middle)]
      [(- maxi middle) maxi])))


(comment 
  " 0 127 (128 values), [0 (/ 128 2) => 64]"
  (get-interval [0 127] "F")
  (get-interval [0 127] "B")
  (get-interval [0 63]  "F")
  (get-interval [0 63]  "B")
  (get-interval [0 7]   "R")
  (get-interval [4 7]   "L")
  )


(defn- red-boardpass
  [mini maxi boardpass-part]
  (loop [[mini maxi] [mini maxi]
         [letter & other] (take 7 boardpass-part)]
    (let [new-min-max (get-interval [mini maxi] letter)]
      (if other
        (recur new-min-max other)
        (first new-min-max)))))

(defn- red-boardpass-row
  [mini maxi boardpass]
  (red-boardpass mini maxi (take 7 boardpass)))

(defn- red-boardpass-col
  [mini maxi boardpass]
  (red-boardpass mini maxi (drop 7 boardpass)))

(defn- boardpass-id
  [boardpass]
  (let [row (red-boardpass-row 0 127 boardpass)
        col (red-boardpass-col 0 7 boardpass)]
    (+ (* 8 row) col)))


;; ==================================================


(defn- solve1
  "first solution"
  [input_]
  (->>  input_
        (map boardpass-id)
        (apply max))
  )


(defn- solve2
    "second solution"
  [input_]
  (->> input_
       (map boardpass-id)
       sort
       (partition-all 2 1)
       (filter #(> (count %) 1))
       (filter (fn [[a b]] (> (- b a) 1)))
       first
       first
       inc
       )
  )


;; entry point from CLI
(defn run [& _]
    (println "Day5 solution1: " (solve1 real-input))
    (println "Day5 solution2: " (solve2 real-input))
    )
