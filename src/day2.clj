(ns day2
  (:require [utils]
            [clojure.string :as str]))


(def demo-input
  ["1-3 a: abcde"
   "1-3 b: cdefg"
   "2-9 c: ccccccccc"])


(def real-input
  (utils/load-input 2 utils/input->list))


(defn- parse-pwd-line
  "Parse a password line from input
  and return the password and its policy
  @return `[\"xxx\" [c 1 4]]`"
  [pwd-line]
  (let [[_ mini maxi letter pwd] (re-find #"(\d+)-(\d+) (\w): (.+)" pwd-line)]
    [pwd [(first letter) (Integer/parseInt mini) (Integer/parseInt maxi)]]
    )
  )


(defn- pwd-valid-sled?
  "Check if a password matches its policy.
  @pwd is a simple string
  @policy is a vector with [letter min max] like `[\"x\" 1 4]`"
  [pwd [letter min max]]
  (let [pwd-groups (->> (group-by identity pwd)
                        (#(into {} (for [[a b] %] [a (count b)]))))
        policy-letter-count (get pwd-groups letter 0)]
    (cond (> policy-letter-count max) false
          (< policy-letter-count min) false
          :else true)
    )
  )


(defn- pwd-valid-toboggan?
  "Check if a password matches its policy.
  @pwd is a simple string
  @policy is a vector with [letter position1 position2] like `[\"x\" 1 4]`"
  [pwd [letter pos1 pos2]]
  (let [lp1 (nth pwd (dec pos1))
        lp2 (nth pwd (dec pos2))
        myxor (fn [a b] (and (or a b) (not (and a b))))]
    (myxor (= letter lp1) (= letter lp2))
    )
  )


(defn solve1
  "Solving the day 2 part 1."
  [input_]
  (->> input_
     (map parse-pwd-line)
     (filter #(apply pwd-valid-sled? %))
     count)
  )


(defn solve2
  "Solving the day 2 part 2."
  [input_]
  (->> input_
     (map parse-pwd-line)
     (filter #(apply pwd-valid-toboggan? %))
     count)
  )


(defn run
  [& _]
  (println "Day2 solution1: " (solve1 real-input)) ; 439
  (println "Day2 solution2: " (solve2 real-input)) ; 584
  ) 

