(ns day9
  (:require
   [utils]
   [clojure.string :as str]))

;; ==================================================

(defn- parse-input
  [raw-input]
  (->> raw-input
      str/split-lines
      (map read-string)))

(def demo-input (parse-input "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576"))

(def real-input (utils/load-input 9 parse-input))

;; ==================================================


;; :input [1 2 3 ] 4
;; :output [1 3 4] or nil if not found
(defn- is-sum-of?
  "Tell if a `target` is the sum of 2 entries in `numbers`.
  Return format [number1 number2 total]."
  [numbers target]
  (let [possibilities 
        (for [x numbers, y numbers
              :let [sum (+ x y)]
              :when (and (not (= x y)) (= target sum))]
          [x y target])]
    (first possibilities))
  )


(defn- find-non-sum
  [numbers width]
  (loop [start 0]
    (let [slice (->> numbers (drop start) (take width))
          target (nth numbers (+ start width) nil)]
      ;; (println target)
      (cond (nil? target)
            nil ;; end of the list acheived
            
            (nil? (is-sum-of? slice target))
            target ;; return the wrong number
            
            :else
            (recur (inc start))
            ))))

(comment
  (find-non-sum demo-input 5)
  )

(defn solve1
  "Solution of part 1"
  [input width]
  (find-non-sum input width))


;; ==================================================


(defn- search-target
  "Search for consecutive numbers in `values` totaling `target`."
  [values target]
  (reduce 
   (fn [{total :total pointer :pointer} val]
     (let [new-acc (+ total val)]
       (cond
         (= target new-acc)
         (reduced {:found pointer})

         (> new-acc target)
         (reduced nil)
         
         :else
         {:total new-acc :pointer (inc pointer)})))
   {:total 0 :pointer 1}
   values
   )
  )


(defn- solve2
  "Solution of part 2, takes solution from part 1 as the second input"
  [input target]
  (->> (range (count input))
       (some (fn [start]
               (let [result (search-target (drop start input) target)]
                 (if result
                   (take (:found result) (drop start input)))))
             ,,,)
       (apply (juxt min max))
       (apply +)
       )
  )


(defn run
  "Run the solution for the day."
  [& _]
  (println "Day 9 solution part 1:" (solve1 real-input 25)) ;; 1930745883
  (println "Day 9 solution part 1:" (solve2 real-input 1930745883)) ;; 268878261
  )
