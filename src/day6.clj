(ns day6
  (:require [utils]
            [clojure.string :as str]))

(def demo-input "abc

a
b
c

ab
ac

a
a
a
a

b") ; => 11


(def real-input
  (utils/load-input 6 identity))


(defn- split-group-answers
  "Makes a raw group answers text into a list of personal answers."
  [group-answers]
  (->> group-answers
      str/split-lines
      (map #(apply list %))
      flatten
      frequencies
      ))

(defn- minch-group-answers
  "Breaks full answers text into a list of answer per group of people."
  [raw-input]
  (->> (str/split raw-input #"\n\n")
       (map split-group-answers)
       (map count)
       (apply +))
  )


(defn- solve1
  "first solution"
  [input_]
  (->> input_
       minch-group-answers))


;; ==================================================


(defn- count-same-answers-in-group
  [group-answers]
  (let [persons (count group-answers)]
    (->> group-answers
         (map #(apply list %))
         flatten
         frequencies
         (filter #(= persons (val %)))
         count
         )
    )
  )


(defn- solve2
  "second solution"
  [input_]
  (->> (str/split input_ #"\n\n")
       (map #(apply list (str/split-lines %)))
       (map count-same-answers-in-group)
       (apply +)
       )
    )


;; entry point from CLI
(defn run [& _]
    (println "Day6 solution1: " (solve1 real-input)) ; => 6549
    (println "Day6 solution2: " (solve2 real-input)) ; => 3466
    )
