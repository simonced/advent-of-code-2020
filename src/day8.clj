(ns day8
  (:require [utils]
            [clojure.string :as str]))


(defn- parse-input-line
  [line]
  (let [[ope arg] (str/split line #" ")]
    [ope (read-string arg)]))

(defn- parse-input-lines
  [lines]
  (vec (map parse-input-line lines))
  )


(def demo-input-raw "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6")

(def demo-input (->> demo-input-raw str/split-lines parse-input-lines))

(def real-input (utils/load-input 8 (comp parse-input-lines utils/input->list)))

;; ==================================================

(defstruct boot-state :step :acc :history)


(defn- update-state
  [state new-acc old-step new-step]
  (assoc state
         :step new-step
         :acc new-acc
         :history (conj (:history state) old-step)))

;; (update-state (struct boot-state 0 0 (empty set)) 6 123 124)


(defn- boot-run
  [boot]
  (loop [state (struct boot-state 0 0 #{})
         current-line 0
         [instruction arg] (first boot)]

    ;; (prn (assoc state :history nil) instruction arg)
    ;; (prn current-line)
    
    (cond ((:history state) current-line)
          (assoc state :reason :infinite-loop)

          (>= current-line (count boot))
          (assoc state :reason :success)

          (= "acc" instruction)
          (recur (update-state state (+ (:acc state) arg) current-line (inc current-line))
                 (inc current-line)
                 (nth boot (inc current-line) nil))

          (= "jmp" instruction)
          (recur (update-state state (:acc state) current-line (+ current-line arg))
                 (+ current-line arg)
                 (nth boot (+ current-line arg) nil))

          (= "nop")
          (recur (update-state state (:acc state) current-line (inc current-line))
                 (inc current-line)
                 (nth boot (inc current-line) nil)))
    )
  )


(defn- solve1
  [input]
  (:acc (boot-run input)))


;; ==================================================


(defn- find-nop-and-jump
  "Searching for nope and jumps, making it a list [position instruction argument]"
  [input]
  (->> input
       (keep-indexed (fn [index [instr arg]]
                       (if (#{"nop" "jmp"} instr)
                         [index instr arg])))
       )
  )


(defn- swap-instruction
  [[pos instr arg]]
  [pos (if (= instr "nop") "jmp" "nop") arg])

(comment
  (swap-instruction [2 "jmp" 4])
  (swap-instruction [3 "nop" 8])
  )


(defn- modify-boot
  [boot line new-instr]
  (let [[_ current-arg] (nth boot line)]
    (assoc boot line [new-instr current-arg])))


(defn- solve2
  [boot]
  (let [change-instructions (find-nop-and-jump boot)]
    (->> (map
          (fn [[pos instr _]] (boot-run (modify-boot boot pos instr)))
          (map swap-instruction change-instructions))
         (sort-by (fn [[state & _]] (:step state)))
         (some #(if (= :success (:reason %)) %))
         :acc
         ))
  )


(defn run [& args]
  (println "Day 8 solution part 1:" (solve1 real-input)) ; => 1337
  (prn "Day 8 solution part 2: " (solve2 real-input)) ; => 1358
  )
