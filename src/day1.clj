(ns day1
  (:require [utils]))

(def demo-input [1721
                 979
                 366
                 299
                 675
                 1456])

(def real-input
  (->>
   (utils/load-input 1 utils/input->list)
   (map #(Integer/parseInt %))))


(defn- cleanup-candidates
  [candidates]
  (->> candidates
       (map sort)
       set
       first))


(defn get-2020-pair
  "Find the pair that makes 2020."
  [input_]
  (->>
   (for [x input_
         y input_
         :when (= 2020 (+ x y))]
     [x y])
   cleanup-candidates))


(defn get-2020-trio
  "Find the trio that sums to 2020."
  [input_]
  (->>
   (for [x input_
         y input_
         z input_
         :when (= 2020 (+ x y z))]
     [x y z])
   cleanup-candidates))



(comment
  "testing difference between :when and :while..."
  
  (time (dorun
         (for [x real-input
               y real-input
               z real-input
               :when (= 2020 (+ x y z))]
           [x y z])))

  (time (dorun
         (for [x real-input
               y real-input
               z real-input
               :while (not= 2020 (+ x y z))]
           [x y z])))
  )


(defn solve1
  "Solve day1 with input."
  [input_]
  (->> input_
       get-2020-pair
       (apply *)))


(defn solve2
  "Solve day1 with input."
  [input_]
  (->> input_
       get-2020-trio
       (apply *)))


(defn run
  [& _]
  (println "Day1 solution 1: " (solve1 real-input))
  (println "Day1 solution 2: " (solve2 real-input)))
