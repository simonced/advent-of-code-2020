(ns day10
  (:require [utils]))

(def demo-input [16 10 15 5 1 11 7 19 6 12 4])
(def demo-input-long [28 33 18 42 31 14 46 20 48 47 24 23 49 45 19 38 39 11 1 32 25 35 8 17 7 9 4 2 34 10 3])

(defn device-high-joltage
  [adapters]
  (+ 3 (apply max adapters)))

(comment
  (device-high-joltage demo-input)
  )

(def real-input (map read-string (utils/load-input 10 utils/input->list)))

;; ======================================================

(defn make-adapters-chain
  "Connect all adapters `input` in a chain and return that chain.
  That chain contains the starting 0 joltage AND the device max joltage."
  [input]
  (->> (conj input (device-high-joltage input))
       (cons 0)
       sort))

(defn count-chain-diff
  "Count the difference between 2 adapters in the chain"
  [chain]
  (->> chain
       (partition 2 1)
       (map (fn [[a b]] (- b a)))))

(defn- solve1
  [input]
  (->
    input
    make-adapters-chain
    count-chain-diff
    frequencies
   (#(* (% 1) (% 3)))
   ))

;; ==================================================

(comment
  "0 1 4 5 6 7 10 11 12 15 16 19 22"
  "0 1 4   6 7 10 11 12 15 16 19 22"
  "0 1 4     7 10 11 12 15 16 19 22"
  "..."
  (->> demo-input
       make-adapters-chain
       ;(take-while some? (iterate next))
       )
  )


(defn can-remove?
  "Check if `a` and `c` are enough and `b` can be removed."
  [a _b c]
  (<= 1 (- c a) 3))


(defn remove-one
  "Remove one adapter from the chain or nil if impossible.
  `chain` has to be already sorted."
  [chain]
  (let [triplets (partition 3 1 chain)]
    (map #(apply can-remove? %) triplets)
    ;; continue
    )
  )

(defn exp [x n]
  (reduce * (repeat n x)))

(comment
  (let [tmp (make-adapters-chain demo-input-long)]
    (->> (remove-one tmp)
         (filter true?)
         count
         (exp 2))
    )
  )


(defn find-possibilities
  "Find other alternatives to a given chain (sorted and containing 0 and device max)."
  [acc chain]
  (let [shorter (remove-one chain)]
    ;(if shorter
    ;  (recur (conj acc shorter) shorter)
    ;  acc)
    )
  )


(defn- solve2
  [input]
  ;; TODO
  )

;; ==================================================

(defn run [& _]
  (println "Day 10 solution, part 1:" (solve1 real-input)) ;; 1904
  ;; (println "Day 10 solution, part 2:" (solve2 real-input))
  )
