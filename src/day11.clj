(ns day11
  (:require [utils]
            [clojure.string :as str]))


(def demo-input "L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL")


(def real-input (utils/load-input 11 identity))

(defn input->grid-data
  [input]
  (let [lines (str/split-lines input)
        height (count lines)
        width (count (first lines))]
    ;; {:width width :height height :grid (into [] (mapcat identity lines))}
    {:width width :height height :grid (into [] (str/join lines))}
    ))


(defn count-occupied-seats
  [grid]
  (count (filter #{\#} grid)))


(defn grid-location
  [width x y]
  (+ x (* width y)))


(defn in-boundaries
  "Restrict coordinates [x1 y1, x2 y2] into a square that fits in the grid."
  [grid-data x1 y1 x2 y2]
  (let [{w :width h :height g :grid} grid-data
        [ax ay] [(max  0      x1)      (max 0  y1)]
        [bx by] [(min (dec w) x2) (min (dec h) y2)]]
    [ax ay bx by])
  )


(defn get-at
  [{w :width g :grid} x y]
  (let [location (grid-location w x y)]
    (nth g location))
  )


(defn get-area
  [grid-data x y]
  (let [[x1 y1 x2 y2] (in-boundaries grid-data (dec x) (dec y) (inc x) (inc y))]
    (for [ny (range y1 (inc y2))
          nx (range x1 (inc x2))]
      {:x nx
       :y ny
       :status (get-at grid-data nx ny)}
      )
    )
  )


(defn get-around
  [grid-data x y]
  (let [area (get-area grid-data x y)]
    ;; (filter #(and (not= (:x %) x) (not= (:y %) y)) area)
    (remove #(and (= (:x %) x) (= (:y %) y)) area)
    )
  )

(comment
  (let [{w :width g :grid :as grid-data} (input->grid-data demo-input)]
    ;; (nth g (grid-location w 9 9))
    (get-area grid-data 9 9)
    ;; (get-around grid-data 9 9)
    )
  )


(defn seat-free?
  [location-status]
  (= \L location-status))


(defn seat-occupied?
  [location-status]
  (= \# location-status))


(defn is-floor?
  [location-status]
  (= \. location-status))


(defn update-grid
  "Takes the grid-data as argument, and return the new location status at x y coordinates."
  [grid-data x y]
  (let [self (get-at grid-data x y)
        area (remove is-floor? (map :status (get-area grid-data x y)))
        around (remove is-floor? (map :status (get-around grid-data x y)))]
    (cond      
      (every? seat-free? area)
      \#

      (and (seat-occupied? self) (<= 4 (count (filter seat-occupied? around))))
      \L

      :else
      self)))


(defn tick
  [f {w :width h :height grid :grid :as grid-data}]
  (assoc grid-data :grid 
         (for [y (range 0 h)
               x (range 0 w)]
           (f grid-data x y))))


(defn solve1
  [input]
  (let [grid-data (input->grid-data input)]
    (loop [gd grid-data]
      (let [tgd (tick gd)]
        (if (= (:grid gd) (:grid tgd))
          (->> tgd
               :grid
               ;; (map :status)
               (remove is-floor?)
               (filter seat-occupied?)
               count)
          (recur tgd))))))


(comment
  (->> demo-input
       input->grid-data
       (tick update-grid)
       :grid)
  )

;; ==================================================


(defn is-in-bounds?
  [{w :width h :height} x y]
  (and (<= 0 x (dec w))
       (<= 0 y (dec h)))
  )


(defn look-in-direction
  "px and py are seat coordinates.
  lx and ly are looking vector"
  [{w :width h :height :as grid-data} px py [lx ly]]
  (let [positions (for [step (range)
                        :let [cx (+ px (* lx step))
                              cy (+ py (* ly step))]
                        :while (is-in-bounds? grid-data cx cy)]
                    [cx cy])]
    (->> positions
         rest        ; skipt the first one, that isaaa the px py location
         (pmap (fn [[x y]] (get-at grid-data x y)))
         (remove is-floor?)
         first)
    )
  )


(def all-directions (for [y [-1 0 1] x [-1 0 1] :when (not= 0 x y)] [x y]))


(defn get-around2
  [grid-data x y]
  (let [look-in-grid-direction (partial look-in-direction grid-data x y)]
    (->> (map look-in-grid-direction all-directions)
         (remove nil?)
         )
    )
  )


(comment
  (-> demo-input
      input->grid-data
      (get-around2 9 9)
      )
  )


(defn update-grid2
  [grid-data x y]
  (let [self (get-at grid-data x y)
        around (get-around2 grid-data x y)]
    ;; (println around)
    (cond      
      (and (seat-free? self) (every? seat-free? around))
      \#

      (and (seat-occupied? self) (<= 5 (count (filter seat-occupied? around))))
      \L

      :else
      self))
  )


(comment
  (->> demo-input
       input->grid-data
       (tick update-grid2)
       (tick update-grid2)       
       :grid)
  )


(defn solve2
  [input]
  (let [grid-data (input->grid-data input)]
    (loop [gd grid-data]
      (let [tgd (tick update-grid2 gd)]
        (if (= (:grid gd) (:grid tgd))
          (->> tgd
               :grid
               (remove is-floor?)
               (filter seat-occupied?)
               count)
          (recur tgd))))))


;; ==================================================


(defn run [&_]
  (println "Day 11 solutions take a very long time so here is a text version of the solutions: 2346 and 2111");
  ;; (println "Day 11 solution 1:" (solve1 real-input)) ; 2346 (took forever!)
  ;; (println "Day 11 solution 2:" (solve2 real-input)) ; 2111 (took very long as well!)
  )
