(ns day7
  (:require [utils]
            [clojure.string :as str]))

(def my-bag "shiny gold")

(def demo-input
  ["light red bags contain 1 bright white bag, 2 muted yellow bags."
   "dark orange bags contain 3 bright white bags, 4 muted yellow bags."
   "bright white bags contain 1 shiny gold bag."
   "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags."
   "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags."
   "dark olive bags contain 3 faded blue bags, 4 dotted black bags."
   "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags."
   "faded blue bags contain no other bags."
   "dotted black bags contain no other bags."
   ])

(def real-input
  (utils/load-input 7 utils/input->list))

;; ==================================================

(defn- parse-bag-content
  [content]
  (let [content (str/replace content #" bags?" "")
        parts (str/split content #", ")]
    (->> parts
           (map #(re-find #"^(\d+) (.+)" %))
           (map rest)
           (map (fn [[qty content]] [(Integer/parseInt qty) content]))
           )
    )
  )


(defn- parse-bag-rule
  [rule]
  (let [[_ outer-bag content] (re-find #"^([\w\s]+) bags contain (.*)\.$" rule)]
    {outer-bag (if (str/starts-with? content "no")
                 '()
                 (parse-bag-content content))}))


(def demo-bags-rules (into {} (map parse-bag-rule demo-input)))
(def all-bags-rules (into {} (map parse-bag-rule real-input)))


(defn- bag-contains-color?
  [bag-container my-color]
  (->> bag-container
       all-bags-rules
       (some (fn [[_qty color]]
               (or (= color my-color)
                   (bag-contains-color? color my-color))))))


(defn- solve1
  "first solution"
  [bag]
  (->> all-bags-rules
       (filter (fn [[container content]]
                 (bag-contains-color? container bag)))
       count)
    )

;; ==================================================


(comment
  "         container      content"
  "-------------------------------------------------------------"
  :sample {"muted yellow" ([2 "shiny gold"] [9 "faded blue"])
           "light red"    ([1 "bright white"] [2 "muted yellow"])
           "dotted black" ()
           "dark orange"  ([3 "bright white"] [4 "muted yellow"])
           "bright white" ([1 "shiny gold"])
           "shiny gold"   ([1 "dark olive"] [2 "vibrant plum"])
           "faded blue"   ()
           "vibrant plum" ([5 "faded blue"] [6 "dotted black"])
           "dark olive"   ([3 "faded blue"] [4 "dotted black"])
           }
  
  )

(defn- solve2
  [color]
  (->> color
       all-bags-rules
       (reduce (fn [acc [qty next-color]]
                 (+ acc qty (* qty (solve2 next-color))))
               0))
  
  )

;; entry point from CLI
(defn run [& _]
    (println "Day1 solution1: " (solve1 my-bag)) ; => 112
    (println "Day1 solution2: " (solve2 my-bag)) ; => 6260
    )
