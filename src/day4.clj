(ns day4
  (:require [utils]
            [clojure.string :as str]))

;; https://adventofcode.com/2020/day/4


(def expected-fields ["byr" ; (Birth Year)
                      "iyr" ; (Issue Year)
                      "eyr" ; (Expiration Year)
                      "hgt" ; (Height)
                      "hcl" ; (Hair Color)
                      "ecl" ; (Eye Color)
                      "pid" ; (Passport ID)
                      ;; "cid" ; (Country ID)
                      ])


(defn- between? [val mini maxi]
  (<= mini val maxi))


(defn- check-byr [val]
  (-> (Integer/parseInt val)
      (between? ,,, 1920 2002)))

(defn- check-iyr [val]
  (-> (Integer/parseInt val)
      (between? ,,, 2010 2020)))

(defn- check-eyr [val]
  (-> (Integer/parseInt val)
       (between? ,,, 2020 2030)))

(defn- check-hgt [val]
  (let [[_ height unit] (re-find #"^(\d+)(cm|in)$" val)
        int-or-0 (fn [x] (if (nil? x) 0 (Integer/parseInt x)))
        heightv (int-or-0 height)]
    (cond
      (= unit "cm") (between? heightv 150 193)
      (= unit "in") (between? heightv 59 76)
      :else false)))

(defn- check-hcl [val]
  (->> val
       (re-matches #"^#[0-9a-f]{6}$")
       nil?
       not))

(defn- check-ecl [val]
  (some #(= % val)
        ["amb" "blu" "brn" "gry" "grn" "hzl" "oth"]))

(defn- check-pid [val]
  (->> val
       (re-matches #"^\d{9}$")
       nil?
       not))

(def fields-checks
  {"byr" check-byr
   "iyr" check-iyr
   "eyr" check-eyr
   "hgt" check-hgt
   "hcl" check-hcl
   "ecl" check-ecl
   "pid" check-pid})

;; step 1 sample

(def demo-input-raw
  "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in")

;; step 2 samples

(def demo-invalid
  "eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007")

(def demo-valid
  "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719")


(defn- parse-passport
  [passport]
  (let [blocks (str/split passport #"[\n ]")]
    (->> blocks
         (map #(str/split % #":"))
         (into {}))))


(defn- parse-input
  [raw]
  (let [passports (str/split raw #"\n\n")]
    (->> passports
         (map parse-passport))))


(def demo-input
  (->> demo-input-raw parse-input))


(def real-input
  (->> (utils/load-input 4 parse-input)))


(defn- has-all-fields?
  "Passeport contains all fields?"
  [passport]
  ;; (every? #() passport)
  (->> expected-fields
       (map #(get passport %) ,,,)
       (some nil?)
       not
       )
  )


(defn- solve1
  "first solution"
  [passports]
  (->> passports
       (map has-all-fields?)
       (filter true?)
       count)
  )

(comment
  (solve1 demo-input)
  )


(defn- run-validation
  "Run a validation."
  [[f-name value]]
  (let [f (get fields-checks f-name)]
    (cond
      (= "cid" f-name) true
      (nil? f) false
      :else (f value))
    ))


(defn- valid?
  "Checks if passport values are valid.
  To be called after checking all fields are present."
  [passport]
  (->> passport
       (map run-validation)
       (reduce #(and %1 %2) true)
       )
  )


(defn- solve2
  "second solution"
  [passports]
  (->> passports
       (filter has-all-fields?)
       (filter valid?)
       count)  
  )

;; entry point from CLI
(defn run [& _]
  (println "Day4 solution1: " (solve1 real-input)) ; 264
  (println "Day4 solution2: " (solve2 real-input)) ; 224
  )
