(ns day3
    (:require [utils]))


(def demo-input
  [
   "..##......."
   "#...#...#.."
   ".#....#..#."
   "..#.#...#.#"
   ".#...##..#."
   "..#.##....."
   ".#.#.#....#"
   ".#........#"
   "#.##...#..."
   "#...##....#"
   ".#..#...#.#"
   ])

(def real-input
  (utils/load-input 3 utils/input->list))


(defn- wrap-x
  "Calculate x position on the map by wrapping around.
  @input is the full map we simply use to calculate the width.
  @x position starting at 1.
  @return x between 1 and width of input"
  [input x]
  (let [width (-> input first count)
        xw (mod x width)]
    (if (= 0 xw) width xw)
    )
  )


(defn- get-at
  "Get map entry at position x y"
  [input x y]
  (let [line (nth input (dec y))]
    (nth line (dec x))
    )
  )


(defn- is-position-tree?
  "Check if a given position is a tree on a map"
  [input x y]
  (= \# (get-at input x y))
  )


(defn- calculate-slopes
  "Calculate all the slopes with the settings sx and sy."
  [input sx sy]
  (for [y (range 1 (inc (count input)))
        :let [x (->> (* y sx) inc (wrap-x input))]
        :while (< (* y sy) (count input))]
    [x (inc (* y sy))]
    )
  )


(defn- solve1
  "first solution"
  [input sx sy]
  (->> (calculate-slopes input sx sy)
       (map (fn [[x y]] (is-position-tree? input x y)))
       (filter true?)
      count
      )
  )


(def possibilities
  [[1 1]
   [3 1]
   [5 1]
   [7 1]
   [1 2]])


(defn- solve2
  "second solution"
  [input_ possibilitties_]
  (->> possibilitties_
       (map (fn [[x y]] (solve1 input_ x y)))
       (apply *)
       )
  )


; entry point from CLI
(defn run
  [& _]
  (println "Day3 solution1: " (solve1 real-input 3 1)) ; 167
  (println "Day3 solution2: " (solve2 real-input possibilities)) ; 736527114
  )

