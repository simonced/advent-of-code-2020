# Simple usage

For running day1 problems:

```sh
clj -X day1/run
```

# Template

In the example below, change `day1` and `1` for the new day...
```clj
(ns day1
	(:require [utils]))

(def demo-input [
	 ...
	])

(def real-input
  (utils/load-input 1 utils/input->list))

;; TODO different tools to parse and deal w/ data

(defn- solve1
	"first solution"
	[input_]
	;; TODO
	)
	
(defn- solve2
	"second solution"
	[input_]
	;; TODO
	)

;; entry point from CLI
(defn run [& _]
	(println "Day1 solution1: " (solve1 real-input))
	(println "Day1 solution2: " (solve2 real-input))
	)
```
