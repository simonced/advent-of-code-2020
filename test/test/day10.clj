(ns test.day10
  (:require [clojure.test :refer :all])
  (:use [day10]))

(deftest part1
  (let [test-chain [2 6 4]]
    (testing "device joltage"
      (is (= (device-high-joltage day10/demo-input)
             22)))

    (testing "chain adapters"
      (is (= (make-adapters-chain test-chain)
             [0 2 4 6 9])))

    (testing "chain differences"
      (is (= (-> test-chain make-adapters-chain count-chain-diff)
             [2 2 2 3]))))
  )

(deftest part2
  ; (0) 1 2 4 6 (9) => (0) 1 4 6 (9)
  (let [test-chain [1 2 6 4]
        sorted-chain (-> test-chain make-adapters-chain)]

    (testing "check adapters difference"
      (is (= (can-remove? 0 1 2)
             true))
      (is (= (can-remove? 0 1 3)
             true))
      (is (= (can-remove? 0 1 4)
             false))
      )

    (testing "removing"
      (is (= (remove-one [0 3 6 9])
             nil))
      (is (= (remove-one sorted-chain)
             [0 1 4 6 9])))

    (testing "possibilities"
      (is (= (find-possibilities [] [0 1 2 3 6 8 9])
             [[0 1 3 6 8 9]
              [0 1 3 6 9]
              [0 2 3 6 8 9]
              [0 2 3 6 9]
              [0 3 6 8 9]
              [0 3 6 9]]))
      )
    ))